import aiofiles

from pathlib import Path
from typing import Annotated
from fastapi import FastAPI, File, UploadFile


BASE_DIR = Path(__file__).resolve().parent
FILE_DIR = BASE_DIR / "files"

app = FastAPI()


@app.post("/files/")
async def create_upload_file(
    file: Annotated[UploadFile, File(description="A file read as UploadFile")],
):
    async with aiofiles.open(f"{FILE_DIR}/{file.filename}", "wb+") as new_file:
        file_content = await file.read()
        await new_file.write(file_content)
    return {"filename": file.filename}


@app.get("/files/")
async def list_files():
    files = FILE_DIR.glob("*")
    return {"files": [file.name for file in files]}
